<?php 

    require '../function.php';

    session_start();

    if(isset($_SESSION['id'])){

        $id = $_SESSION['id'];
        $access = mysqli_query($conn, "SELECT access FROM user WHERE id = $id");
        $user = mysqli_fetch_assoc($access);

        if (isset($_SESSION["login"])){

            if($user["access"] !== "user"){
                header("Location: ../index.php");
                exit;
            }
        }
    }

    if (!isset($_SESSION["login"])){
        header("Location: ../index.php");
        exit;
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman User</title>
</head>
<body>
    <h1>Anda User</h1>
    <a href="../logout.php">Logout</a>
</body>
</html>