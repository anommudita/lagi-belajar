<?php 
    require 'function.php';

    session_start();

    if(isset($_POST["sigh-in"])){

        if(registrasi($_POST) > 0){

            $_SESSION["login"] = true;
            echo "<script>
                    alert('Username telah ditambahkan!');
                    document.location.href = 'recrut.php'
                  </script>";
            
        }else{
            echo mysqli_error($conn);
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrasi</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/regist.css">
    <?= $icon ?>
</head>
<body>
    <div class="semua">
    <div class="judul"><h1 class="text-center">Registrasi</h1></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4 kotak">
            <div class="col-sm-12 text-center">
                <form action="" method="POST">
                    <label for="username">Username</label>
                    <br>
                    <input type="text" name="username" id="username" placeholder="Massukkan Username" required>
                    <br>
                    <label for="email">Email</label>
                    <br>
                    <input type="text" name="email" id="email" placeholder="Massukkan Email" required>
                    <br>
                    <label for="password">Password</label>
                    <br>
                    <input type="password" name="password" id="password" placeholder="Massukkan Password" required>
                    <br>
                    <label for="password2">Confirm Password</label>
                    <br>
                    <input type="password" name="password2" id="password2" placeholder="Tulis Ulang Password" required>
                    <br>
                    <button type="submit" name="sigh-in" class="btn btn-primary">Sigh-in</button>
                </form>
            </div>
            </div>
        </div>
    </div>
    </div>
    <footer class="text-center">
        <p>&copy; 2021, create by Yuda aditya.</p>
    </footer>
</body>
</html>